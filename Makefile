CXX=g++
CXXFLAGS=-Wall -Wextra -std=c++17

SRCS=$(wildcard *.cpp)
OBJS=$(patsubst %.cpp, %.o, $(SRCS))
TESTS=$(wildcard tests/*.cpp)
TEST_OBJS=$(patsubst %.cpp, %.o, $(TESTS))
EXE=cw

run: $(EXE)
	./$(EXE)

all: $(EXE)

$(EXE): $(OBJS)
	$(CXX) $(OBJS) -o $(EXE)

test: $(TEST_OBJS)
	$(CXX) $(filter-out main.o,$(OBJS)) $(TEST_OBJS) -o testing
	./testing

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

testing/%.o: tests/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	$(RM) $(EXE)
	$(RM) *.o

.PHONY: all run clean test
