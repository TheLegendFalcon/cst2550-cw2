#ifndef _BOOK_H_
#define _BOOK_H_
#include <iostream>

/*
 Book.h
M00696479
Created:26/4/2021
Updated:28/4/2021
*/

class Book
{
  //A private class to only declare objects inside the class//
  //Author, Title, Qty and ISBN remain for member functions for access// 
private:
  std::string author;
  std::string ISBN;
  std::string title;
  int qty;

  //A public class for data members, in this case the object Book//
public:
  Book();
  Book(std::string title, std::string author, std::string ISBN, int qty);
  //Declaring that the book gets the ISBN, Qty, Author and Title whiel it can also reduce Quantity//
  std::string getISBN();
  int getQty();
  void setQty(int);
  void reduceQty();
  std::string getAuthor();
  std::string getTitle();

  //Display function declared to show the book in the main menu.//
  void Display();
  //Writes into the TSV file//
  std::string writeTsv();
};
#endif
