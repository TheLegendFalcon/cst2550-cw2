#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <fstream>
#include "Book.h"
#include "util.h"

/*
main.cpp
M00696479
Created:26/4/2021
Updated:28/4/2021
*/
/*----------------------------------Menu Function----------------------------------*/

//Making a "books file"
std::string file = "books";
//Declaring the data structure 
LinearProbingHash<std::string, Book> Mybooks;

void menu();
void ChangeFile();
void ReadData();
void SearchItem();
void RemoveItem();
void GetItemData();
void WriteData();
std::string tolower(std::string str);

//This declares and prints out the menu for choosing options for the Library//
void menu()
{
    int x;
    system("clear");
    std::cout << "\n\n\t\t=================== Welcome to Library Management System portal ===================\n\n";
    std::cout << "\n\t\t\t Please choose what you want\n";
    std::cout << "\t\t\t1. Search Item\n";
    std::cout << "\t\t\t2. Add New Item\n";
    std::cout << "\t\t\t3. Remove Item\n";
    std::cout << "\t\t\t4. Load File\n";
    std::cout << "\t\t\t0. Exit the portal\n";
    std::cout << "\n\n\t-->> Enter your choice only number..\n\t--> ";
    std::cin >> x;
    //Switch case used to break down and declare each part of the options//
    switch (x)
    {

    case 1:
        SearchItem();
        break;

    case 2:
        GetItemData();
        break;

    case 3:
        RemoveItem();
        break;

    case 4:
        ChangeFile();
        break;

    case 0:
        exit(0);
        break;

    default:
        std::cout << "\n\n\t\t\tYou have entered a wrong choice. Please try again";
        menu();
        break;
    }
}
//This void changes the file necessary to whatever data file it needs to change to//
void ChangeFile()
{
    std::string temp;

    std::cout << "\n\t\t Enter the File to change to: ";
    std::cin.ignore();
    std::cin >> temp;
    //This rewrites the file and changes the file to the newer selected one//
    WriteData();
    Mybooks.clear();
    file = temp;
    std::cout << "\n\t\t\t File Changed!";
    //It reads data from the new selected file//
    ReadData();
    std::cout << "\n\n\t\t     Enter to go back to Main Menu:";
    std::cin.get();
    std::cin.ignore(10, '\n');
    menu();
}

void ReadData()
{

    std::ifstream Read(file);
    if (!Read)
    {
        int choice;
        std::cout << "\n\n\t\t File Empty ";
        std::cout << "\n\n\t\t 1. Add Books ";
        std::cout << "\n\t\t 2. Change File ";
        std::cout << "\n\t\t Enter a Choice: ";
        std::cin >> choice;
        if (choice == 1)
            GetItemData();
        else if (choice == 2)
            ChangeFile();
        else
        {
            std::cout << "\n\n\n\t\t\t Wrong Choice Entered! Try Again";
            //close fstream
            Read.close();
            ReadData();
        }
        std::cout << "\n\n\t\t Press Enter Key To Continue.";
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    else
    {
        std::string line, word;
        vec<std::string> row;

        while (!Read.eof())
        {
            row.clear();
            getline(Read, line);
            if (line == "")
                continue;
            std::stringstream s(line);
            while (getline(s, word, '\t'))
                row.push_back(word);

            Mybooks.insert(tolower(row[0]), Book(row[0], row[1], row[2], stoi(row[3])));
        }
        Read.close();
    }
}

void SearchItem()
{
    std::string title;

    std::cout << "\n\t\t Enter the Title to Search: ";
    std::cin.ignore();
    getline(std::cin, title);

    title = tolower(title);

    if (Mybooks.contains(title))
    {
        std::cout << "\n\t\t The Title Details" << std::endl;
        Mybooks[title].Display();
    }
    else
        std::cout << "\n\t\t The Title has No Details Found! " << std::endl;

    std::cout << "\n\n\t\t Enter to go back to Main Menu:";
    std::cin.get();
    menu();
}

void RemoveItem()
{
    std::string title;
    char ch;
    do
    {
        ch = 'y';
        std::cout << "\n\t\t Enter the Title: ";
        std::cin.ignore();
        getline(std::cin, title);
        std::cout << "\n\t\t Title Entered: " << title << "\n\t\t Remove this? (Y / N) ";
        std::cin >> ch;
    } while (ch == 'N' || ch == 'n');

    title = tolower(title);

    if (Mybooks.contains(title))
    {
        std::cout << "\n\t\t The Title Details" << std::endl;
        Mybooks[title].Display();
        std::cout << "\n\n\t\t Removed Book!" << std::endl;
        if (Mybooks[title].getQty() - 1 == 0)
            Mybooks.remove(title);
        else
            Mybooks[title].reduceQty();
    }
    else
        std::cout << "\n\t\t The Title has No Details Found! " << std::endl;

    std::cout << "\n\n\t\t Enter to go back to Main Menu:";
    std::cin.get();
    std::cin.ignore(10, '\n');
    menu();
}

void GetItemData()
{
    std::string title, Title, author, temp;
    int qty;
    long ISBN = 0;
    char ch;

    std::cout << "\n\n\n\t\t\t Enter Title: ";
    std::cin.ignore();
    getline(std::cin, title);

    Title = tolower(title);
    if (Mybooks.contains(Title))
    {
        std::cout << "\n\t\t\t Book found, Update stock!";
        do
        {
            qty = 0;
            std::cout << "\n\n\t\t\t Enter Quantity ( > 0): ";
            std::cin >> qty;
        } while (qty <= 0);
        Mybooks[Title].setQty(qty);
    }
    else
    {
        do
        {
            ch = 't';
            std::cout << "\t\t\t Enter Author: ";
            std::cin.ignore();
	    std::getline(std::cin, temp);
            std::cout << "\t\t\t Enter next Author(Y or N): ";
            std::cin >> ch;
            if (ch == 'y' || ch == 'Y')
                author += temp + " ;";
            else
                author += temp;
        } while (ch == 'y' || ch == 'Y');
        do
        {
            std::cout << "\t\t\t Enter ISBN ( > 0): ";
	    std::cin.ignore();
            std::cin >> ISBN;
        } while (ISBN <= 0);
        do
        {
            std::cout << "\t\t\t Enter Quantity ( > 0): ";
            std::cin >> qty;
        } while (qty <= 0);

        Mybooks.insert(tolower(title), Book(title, author, std::to_string(ISBN), qty));
    }
    std::cout << "\n\t\t Books Added" << std::endl;

    std::cout << "\n\n\t\t Enter to go back to Main Menu:";
    std::cin.get();
    menu();
}

void WriteData()
{
    std::string tempfile = "temp";
    std::fstream Write(tempfile, std::ios::out | std::ios::app);
    for (const std::string &title : Mybooks.Keys())
        Write << Mybooks.get(title).writeTsv();

    Write.close();
    remove(file.c_str());
    rename(tempfile.c_str(), file.c_str());
}

std::string tolower(std::string str)
{
    std::transform(str.begin(), str.end(), str.begin(),
                   [](unsigned char c) { return std::tolower(c); });
    return str;
}

int main()
{
    ReadData();
    menu();
}
