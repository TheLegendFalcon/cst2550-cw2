#include <iostream>
#include <string>
#include <sstream>
#include "catch.hpp"
#include "../Book.h"
#include "../util.h"

/*
 test_case.cpp
M00696479
Created:26/4/2021
Updated:28/4/2021

 */

LinearProbingHash<std::string, Book> Mybooks;
std::streambuf *original_cin = std::cin.rdbuf();
std::streambuf *original_cout = std::cout.rdbuf();
std::istringstream test_input;
std::ostringstream test_output;

std::string tolower(std::string str)
{
    std::transform(str.begin(), str.end(), str.begin(),
                   [](unsigned char c) { return std::tolower(c); });
    return str;
}

void RemoveItem(std::string title)
{
    title = tolower(title);

    if (Mybooks.contains(title))
    {
        if (Mybooks[title].getQty() - 1 == 0)
            Mybooks.remove(title);
        else
            Mybooks[title].reduceQty();
    }
}

bool SearchItem()
{
    std::string title;

    std::cout << "\n\t\t Enter the Title to Search: ";
    std::cin.ignore();
    getline(std::cin, title);

    title = tolower(title);

    if (Mybooks.contains(title)) 
    {
        Mybooks[title].Display();
        return true;
    }
    else
        return false;
}

TEST_CASE("Test for making string lowercase", "[tolower()]")
{
    REQUIRE(tolower("HARRY") == "harry");
}

TEST_CASE("Test for Removing books", "[RemoveItem()]")
{
    Mybooks.insert("harry", Book("harry", "ABC", "34570", 5));
    RemoveItem("harry");
    REQUIRE(Mybooks.contains("harry") == true);

    Mybooks.clear();
}

TEST_CASE("Test for searching a Book", "[SearchItem()")
{
    Mybooks.insert("harry", Book("harry", "ABC", "34570", 5));

    test_input.clear();
    test_input.str(" harry\n y\n");
    test_output.clear();
    test_output.str("");

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    REQUIRE(SearchItem() == true);

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);
    Mybooks.clear();
}
